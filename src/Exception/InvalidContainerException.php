<?php

declare(strict_types=1);

namespace Blazon\PSR11FlySystem\Exception;

class InvalidContainerException extends \InvalidArgumentException
{
}
